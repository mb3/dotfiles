# git commit template - XN3.
#
# inspired by Karma's commit style => http://karma-runner.github.io/0.8/dev/git-commit-msg.html
#
## single subject line must be < 51 chars
#
# <type>(<optional scope>): <subject>
#
#feat:          => new feature
#fix:           => bug fix
#docs:          => changes to documentation
#style:         => formatting, missing semi colons, etc; no code change
#refactor:      => refactoring production code
#test:          => adding missing tests, refactoring tests; no production code change
#chore:         => updating grunt tasks etc; no production code change

## commit description line(s) must be < 73 chars
#
# 1. uses the imperative, present tense: "change" not "changed" nor "changes"
# 2. includes motivation for the change and contrasts with previous behavior

## processing jira issues => https://confluence.atlassian.com/display/AOD/Processing+JIRA+issues+with+commit+messages
#
# 1. reuse the same jira ISSUE_KEY on as many lines you need
# 2. jira comment line must be < 73 chars or it gets truncated
# 3. no multiline jira comments allowed
# 4. when you #resolve an jira issue, you cannot set the resolution field via commit
#
#JRA-123 #comment ...
#JRA-123 #time 1w 2d 3h 40m [...]
#JRA-123 #resolve
#JRA-123 #close [...]
#