#!/bin/echo Never run directly! Please use => source

# bashrc colors - XN3.

ncf="\e[0m" # remove colors and formatting
white="\e[0;37m" WHITE="${white%m};1m" white_="${white%m};4m" WHITE_="${white%m};4;1m"
black="\e[0;30m" BLACK="${black%m};1m" black_="${black%m};4m" BLACK_="${black%m};4;1m"
red="\e[0;31m" RED="${red%m};1m" red_="${red%m};4m" RED_="${red%m};4;1m"
green="\e[0;32m" GREEN="${green%m};1m" green_="${green%m};4m" GREEN_="${green%m};4;1m"
blue="\e[0;34m" BLUE="${blue%m};1m" blue_="${blue%m};4m" BLUE_="${blue%m};4;1m"
yellow="\e[0;33m" YELLOW="${yellow%m};1m" yellow_="${yellow%m};4m" YELLOW_="${yellow%m};4;1m"
cyan="\e[0;36m" CYAN="${cyan%m};1m" cyan_="${cyan%m};4m" CYAN_="${cyan%m};4;1m"
magenta="\e[0;35m" MAGENTA="${magenta%m};1m" magenta_="${magenta%m};4m" MAGENTA_="${magenta%m};4;1m"

# my dynamic color tables
# inspired by Flogisoft => http://misc.flogisoft.com/bash/tip_colors_and_formatting
colors(){ echo
    for cg in 8 16 ; do
        # color group config
        # attrl = formatting ; fgcl = foreground ; bgcl = background ; tcl = cell length
        [ "${cg}" == "8" ] && attrl="0 1" fgcl="{30..37} 39" bgcl="{40..47} 49" tcl="9" # 8 colors
        [ "${cg}" == "16" ] && attrl="0" fgcl="{90..97}" bgcl="{100..107} 109" tcl="10" # 16 colors
        # table header
        for bgc in $(eval echo ${bgcl}) ; do echo -en "       ${bgc}" ; done
        echo -en "\n   ." # left corner
        for bgc in $(eval echo ${bgcl}) ; do
            for tc in $(eval echo {1..${tcl}}) ; do echo -en "-" ; done # --line
        done
        echo -en ".\n" # right corner
        # table body
        for fgc in $(eval echo ${fgcl}) ; do
            for attr in ${attrl} ; do
                [ "${attr}" -eq "0" ] && echo -en "${fgc} |" || echo -en " ${attr} |"
                for bgc in $(eval echo ${bgcl}) ; do
                    echo -en "\e[${attr};${fgc};${bgc}m ${attr};${fgc};${bgc} "
                done
                echo -e "\e[0m|" # new line
            done
        done
        # table footer
        echo -en "   '" # left corner
        for bgc in $(eval echo ${bgcl}) ; do
            for tc in $(eval echo {1..${tcl}}) ; do echo -en "-" ; done # --line
        done
        echo -e "'\n" # right corner
    done
}