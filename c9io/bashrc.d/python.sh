#!/bin/echo Never run directly! Please use => source

# bashrc python - XN3.

# this configuration file is a problem with use of virtualenv and
# is automatically restored after a few hours through the system
rm -f ~/.pydistutils.cfg

# cache pip-installed packages to avoid re-downloading
export PIP_DOWNLOAD_CACHE=~/.pip/cache

# pip should only run if there is a virtualenv currently activated
export PIP_REQUIRE_VIRTUALENV=true

# to temporarily manage the system/global packages
pips(){
    PIP_REQUIRE_VIRTUALENV=false pip ${@}
}

# to activate python virtual environment
#export VIRTUALENV_BASE=~/${C9_PID}
export VIRTUALENV_BASE=~/.vpyes
activate(){
    rm -f ~/.pydistutils.cfg
    [ "`declare -f -F deactivate`" ] && deactivate
    if [ "${1##*-}" == "cd" ] ; then
        cd "${VIRTUALENV_BASE}/${2##*/}" ; shift
    fi
    source "${VIRTUALENV_BASE}/${1##*/}/bin/activate"
}