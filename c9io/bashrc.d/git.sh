#!/bin/echo Never run directly! Please use => source

# bashrc git - XN3.

# ~/.gitconfig is automatically restored after a few hours through the system!

# custom global user infos (normally this is not necessary!)
#git config --global user.name "${HGUSER}"
#git config --global user.email "${EMAIL}"

# vim and commit messages with only 72 columns is a good thing
git config --global core.editor "vim -c ':set textwidth=72'"

# set global commit template
git config --global commit.template "${DOTFILES}/tpls/gitcommit.tpl"

# set global gitignore
git config --global core.excludesfile "${DOTFILES}/c9io/gitignore_global"

# global git aliases with svn flavor
git config --global alias.st status
git config --global alias.co checkout
git config --global alias.br branch
git config --global alias.up rebase
git config --global alias.ci commit