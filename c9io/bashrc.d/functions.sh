#!/bin/echo Never run directly! Please use => source

# bashrc functions - XN3.

# create directory and change too
mkcd(){
    mkdir -p "${1}" ; cd "${1}"
}

# git init bitbucket project
git-init-bitbucket(){
    REPOSITORY_USER="mb3"
    [ -z "${1}" ] && echo "USAGE: ${FUNCNAME} <PROJECT_NAME>" && return
    mkdir -p ~/"${C9_PID}/${1}" ; cd ~/"${C9_PID}/${1}"
    if [ ! -d ".git" ] ; then
        git init && git remote add origin "git@bitbucket.org:${REPOSITORY_USER}/${1}.git"
    fi
    if [ ! -f ".gitignore" ] ; then
        sed "s/template/${1}/" "${DOTFILES}/tpls/gitignore.tpl" > .gitignore
    fi
    if [ ! -f "LICENSE" ] ; then
        sed "s/\[year\] \[fullname\]/`date +%Y` ${HGUSER}/" "${DOTFILES}/tpls/LICENSE.tpl" > LICENSE
    fi
    if [ ! -f "README.md" ] ; then
        sed "s/# Project Name/# Project ${1}/" "${DOTFILES}/tpls/README.tpl" > README.md
    fi
}

# git init github project
git-init-github(){
    REPOSITORY_USER="mb3dot"
    [ -z "${1}" ] && echo "USAGE: ${FUNCNAME} <PROJECT_NAME>" && return
    mkdir -p ~/"${C9_PID}/${1}" ; cd ~/"${C9_PID}/${1}"
    if [ ! -d ".git" ] ; then
        git init && git remote add origin "git@github.com:${REPOSITORY_USER}/${1}.git"
    fi
    if [ ! -f ".gitignore" ] ; then
        sed "s/template/${1}/" "${DOTFILES}/tpls/gitignore.tpl" > .gitignore
    fi
    if [ ! -f "LICENSE" ] ; then
        sed "s/\[year\] \[fullname\]/`date +%Y` ${HGUSER}/" "${DOTFILES}/tpls/LICENSE.tpl" > LICENSE
    fi
    if [ ! -f "README.md" ] ; then
        sed "s/# Project Name/# Project ${1}/" "${DOTFILES}/tpls/README.tpl" > README.md
    fi
}

# create virtual python2.6 environment
vpyes-2.6(){
    PYTHON_EXE="python2.6"
    [ -z "${1}" ] && echo "USAGE: ${FUNCNAME} <VIRTUALENV_NAME>" && return
    rm -f ~/.pydistutils.cfg
    if [ ! -d "${VIRTUALENV_BASE}/${1}" ] ; then
        virtualenv -p "${PYTHON_EXE}" --no-setuptools --no-pip "${VIRTUALENV_BASE}/${1}" && activate -cd "${1}"
        curl http://python-distribute.org/distribute_setup.py | python && easy_install pip
    fi
}

# create virtual python3.3 environment
vpyes-3.3(){
    [ -z "${1}" ] && echo "USAGE: ${FUNCNAME} <VIRTUALENV_NAME>" && return
    rm -f ~/.pydistutils.cfg
    if [ ! -d "${VIRTUALENV_BASE}/${1}" ] ; then
        pyvenv-3.3 "${VIRTUALENV_BASE}/${1}" && activate -cd "${1}"
        curl http://python-distribute.org/distribute_setup.py | python && easy_install pip
    fi
}