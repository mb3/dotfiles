#!/bin/echo Never run directly! Please use => source

# bashrc aliases - XN3.

# to avoid mistakes
alias rm='rm -i'
alias cp='cp -i'
alias mv='mv -i'

# i don't like [-F]
# it's already typified with colors [--color=auto]
alias ls='ls --color=auto'

# list with shorter numeric user and group IDs [-n]
# print sizes in human readable format [-h]
alias la='ls -alnh'

# cd to workspace root
alias cdw='cd ~/${C9_PID}'

# reload bashrc
alias bashrc='source ~/.bashrc'