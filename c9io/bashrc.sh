#!/bin/echo Never run directly! Please use => source

# bashrc dotfiles c9io - XN3.

export DOTFILES=~/"${C9_PID}/dotfiles"

for i in "${DOTFILES}/c9io/bashrc.d"/*.sh ; do source "${i}" ; done
unset i