# dotfiles
---
my dotfiles

## Installation

#### c9io (Cloud9 IDE)
    # source main bashrc.sh in ~/.bashrc
    $ echo -e "source ~/${C9_PID}/dotfiles/c9io/bashrc.sh >> ~/.bashrc
    $ source ~/.bashrc

    # create virtual python2.6 environment
    $ vpyes-2.6 default-py2.6

    # autoactivate default python2.6 virtual environment
    $ echo -e "activate default-py2.6" >> ~/.bashrc

    # reload ~/.bashrc
    $ bashrc

#### tpls (templates)
! be used by `functions` in bashrc !

## Usage
in german => <https://xn3dot.atlassian.net/wiki/display/XN3CL9>

## Contributing
1. Fork it!
2. Create your feature branch: `git checkout -b my-new-feature`
3. Commit your changes: `git commit -am 'Add some feature'`
4. Push to the branch: `git push origin my-new-feature`
5. Submit a pull request :D

## Release History
_(Nothing yet)_

## License
MIT